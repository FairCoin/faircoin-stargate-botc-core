function list_items(){
  $("#items").html("");
  var t="";
  t+="<div class='row list_items_headline'>";
  t+="<div class='col-lg-2 col-md-3'><b>TIMESTAMP</b></div>";
  t+="<div class='col-lg-4 col-md-4'><b>IBAN</b></div>";
  t+="<div class='col-lg-2 col-md-2 text-right'><b>AMOUNT</b></div>";
  t+="<div class='col-lg-2 col-md-2 text-right'><b>EXCHANGE_PRICE</b></div>";
  t+="<div class='col-lg-2 col-md-3 text-right'><b>FAIRCOIN_AMOUNT</b></div>";
  t+="<div class='col-lg-2 col-md-3'><b>STATUS (ORDER_ID)</b></div>";
  t+="<div class='col-lg-6 col-md-4'><b>FAIRCOIN_ADDRESS</b></div>";
  t+="<div class='col-lg-4 col-md-3'><b>CONCEPT</b></div>";

  t+="</div>";
  $("#items").append(t);

  JJ.sort( function(a,b) {
      return b.botc_pay_in_timestamp - a.botc_pay_in_timestamp;
    }
  );

  JJ.forEach(
    function(v,i){
      var t="";
      t+="<div class='row list_items_table'>";
      t+="<div class='col-lg-2 col-md-3'>" + JSON.stringify( new Date(v.botc_pay_in_timestamp*1000) ).replace(/\"/g,'').replace(/T/g,' ').slice(0,16) + "</div>";
    //  t+="<div class='col-lg-2'>" + v.order_id + "</div>";
      var beneficiary=""; var bic="";
      J.forEach(
        function( w,j ){
          if( w.iban.replace(/ /g,"").toUpperCase() == v.iban ){
            beneficiary = " - " + w.beneficiary;
            bic = (w.bic).replace(/ /g,"");
          }
        }
      );
      if( beneficiary == '' ) beneficiary = " - <font color='red'> (not found!! pls check)</font><br>";
      t+="<div class='col-lg-4 col-md-4'>" + v.iban + " " + bic + " " + beneficiary + "</div>";
      t+="<div class='col-lg-2 col-md-2 text-right'>" + parseFloat(v.eur_amount).toFixed(2) + " &euro;</div>";
      t+="<div class='col-lg-2 col-md-3 text-right'>" + parseFloat(v.exchange_price).toFixed(2) + " &euro;/FAIR</div>";
      t+="<div class='col-lg-2 col-md-3 text-right'>" + parseFloat(v.fair_amount).toFixed(8) + " FAIR</div>";

      var state_info="";
      var state_class="";
      switch(v.status){
        case '??':
          state_info="??_error";
          state_class="btn-danger";
          break;
        case '00':
          state_info="00_created";
          state_class="btn-warning";
          break;
        case '01':
          state_info="01_F_rec_uncf";
          state_class="btn-primary";
          break;
        case '02':
          state_info="02_F_rec_conf";
          state_class="btn-primary";
          break;
        case 'E02':
          state_info="E02_sent_failed";
          state_class="btn-danger";
          break;
        case '03':
          state_info="03_sent_uncf";
          state_class="btn-warning";
          break;
        case 'E03':
          state_info="E03_sent_failed";
          state_class="btn-danger";
          break;
        case '04':
          state_info="04_done";
          state_class="btn-success";
          break;
      }

      t+="<div class='col-lg-2 col-md-3'><button class='btn " + state_class + " btn-sm' onclick='list_items_update(\"" + v.order_id + "\",\"" + v.status + "\", this )'>" + state_info + "</button> &nbsp;" + v.order_id + "</div>";
      t+="<div class='col-lg-6 col-md-4'><a href='https://chain.fair.to/address?address=" + v.fair_address + "'>" + v.fair_address + "</a></div>";
      t+="<div class='col-lg-4 col-md-3'>" + v.concept + "</div>";
      t+="</div>"
      $("#items").append(t);
    }
  );
}


function list_items_update(order_id, status, obj){
  //console.log( order_id, status, obj );
  if( ! $(obj).hasClass("btn-danger") && ! $(obj).hasClass("btn-warning") ) return;

  var c=prompt("Enter code");
  console.log( order_id, status, obj );
  $.post("update_list_items.php",
    {
        order_id: order_id,
        c:c ,
        status: status
    },
    function(data, status){
        alert("Data: " + data + "\nStatus: " + status);
        if( data == "OK" ){
          if( $(obj).hasClass("btn-danger") ){
            $(obj).text("04_done");
            $(obj).toggleClass("btn-danger");
            $(obj).toggleClass("btn-success");
          }
        }
   });

}


function json_load( url, type ){
  var json = null;
  $.ajax({
      'type':"GET",
      'async': false,
      'global': false,
      'cache': false,
      'url': url,
      'dataType': type,
      'success': function (data) {
          json = data;
      }
  });
  return json;
}
