var GS='';
var FAIR_FREEVISION_PRICE=0;

var J=[];  // stargate directory JSON
var JJ=[]; // stargate selected order JSON

var qrcode = new QRCode(document.getElementById("qrcode"), {
  width : 150,
  height : 150,
  colorDark : "#000000",
   colorLight : "aliceblue",
    correctLevel : QRCode.CorrectLevel.H
});

var F=[];

$( document ).ready(function() {

  F = json_load( "functions/get_freevision_price.php", "json" );

  if( F.fv_bid != undefined ){
    FAIR_FREEVISION_PRICE=parseFloat( F.fv_bid );
  } else {
    $(".line1").toggleClass("d-none",true);
    alert( "Can't get freevision market price !! Stargate gateway not available!!" );
  }

  J=json_load( "data/stargate_entries.json","json" );

  if( getQueryVariable("order_id") ) get_by_order_id(getQueryVariable("order_id"));

});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})


function get_by_order_id(order_id){

  order_id=order_id.trim();
  if( ( order_id!=false ) ? order_id.match(/[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]/g) != null : false ){
    console.log( new Date(), 'get_by_order_id', order_id );
    $.post("functions/get_orders_json.php",
      {
          order_id: order_id,
      },
      function(data, status){
        if( status != "success") alert("Data: " + data + "\nStatus: " + status);
        console.log( new Date(), data );
        JJ=JSON.parse(data);
        console.log(new Date(), JJ);
        if( JJ.length == 1 ){

          J.forEach(
            function(v,i){
              if( v.iban.toString().replace(/ /g,"").toUpperCase() == JJ[0].iban ){
                entry_load(v);
              }
            }
          );
          $("#gateway_id").val(JJ[0].iban);

          $("#order_id").text(JJ[0].order_id);
          $("#tokeninfo p").text("You need the token below for checking the state of transaction or getting support from Stargate team!");

          $("#gateway_amount").val(JJ[0].eur_amount).attr("readonly",true);
          $("#gateway_concept").val(JJ[0].concept).attr("readonly",true);;
          $("#fair_address").val(JJ[0].fair_address);
          $("#fair_amount").val(JJ[0].fair_amount);
          $(".container-fluid *").toggleClass("d-none",false);
          $(".line2").toggleClass("goBackground",true);
          $(".line3").toggleClass("goBackground",true);

          $("#gateway_calculate").toggleClass("d-none",true);
          $("#gateway_open").toggleClass("btn-primary",false).toggleClass("btn-success",true).text("Stargate is still open");

          makeCode("faircoin://" + JJ[0].fair_address + "?amount=" + JJ[0].fair_amount);

          if( JJ[0].status.match(/^E.*/g) != null ) alert('Gateway failed! Error:'+JJ[0].status+'\nPlease contact Admin! Telegram @TonyFord');

          checkPayment();

          ChangeUrl("FairCoin Stargate",STARGATE_URL + "index.php?order_id=" + order_id);

          return true;
        }
     });
  }
  return false;
}


function search_stargate(obj){

  console.log( new Date(), 'search_stargate' );
  if( $("#gateway_id").attr("readonly") == "readonly" ) return;
  $(".line2").toggleClass("d-none",true);
  $(".line3").toggleClass("d-none",true);


  clearTimeout(GS);


  if( get_by_order_id($(obj).val()) ){
    return;
  }


  //if( a == "" ) return;

  if( JJ.length == 1 ){
    // prder found -> load details
  } else {
    // no order found -> search in directory
    GS=setTimeout(
      function(){
        search_stargate_directory(obj);
      },1000
    );
  }



}

function search_stargate_directory(obj){
  $("#result").html("");
  $(".line2").toggleClass("d-none",true);
  $("#gateway").toggleClass("d-none",true);
  $("#gateway_beneficiary").toggleClass("d-none",true);
  $("#gateway_address").toggleClass("d-none",true);
  $("#gateway_amount").toggleClass("d-none",true);
  $("#gateway_currency").toggleClass("d-none",true);
  $(obj).toggleClass("is-invalid",true);
  J.forEach(
    function(v,i){
      if( (v.iban).replace(/ /g,"").match(eval("/^" + (obj.value).replace(/ /g,"") + "$/ig") ) != null ){
        entry_load(v);
        $(obj).toggleClass("is-invalid",false);
      }
    }
  );
}

var E={};  // selected entry

function entry_load(v){

  E = JSON.parse(JSON.stringify(v));
  $("#gateway").val("FAIR to SEPA");
  $("#gateway").toggleClass("d-none",false);
  $("#gateway_beneficiary").val(v.beneficiary);
  $("#gateway_beneficiary").toggleClass("d-none",false);
  $("#gateway_address").val(v.iban);
  $("#gateway_address").toggleClass("d-none",false);
  $("#gateway_amount").toggleClass("d-none",false);
  $("#gateway_amount").prop("placeholder","0.00 - " + (LIMIT*FAIR_FREEVISION_PRICE).toFixed(3) );
  $("#gateway_currency").toggleClass("d-none",false);

  // ##### get current free market price ############


  $("#payment_limit").text( (LIMIT*FAIR_FREEVISION_PRICE).toFixed(2) );
  $("#exchange_price").text( FAIR_FREEVISION_PRICE.toFixed(3) );

  $(".line2").toggleClass("d-none",false);
  $(".line3").toggleClass("d-none",false);

  $("#gateway_amount").focus();
  $("#gateway_id").prop("readonly",true);

}


function check_amount(obj){

  //$(".line3").toggleClass("d-none",true);

  if( obj.value == "" ){
    $(obj).toggleClass("is-invalid",true);
    $(obj).toggleClass("is-valid",false);
    $("#gateway_amount_invalid").text("please enter an amount");
    return;
  }

  if( $.isNumeric(obj.value) == false ){
    $(obj).toggleClass("is-invalid",true);
    $(obj).toggleClass("is-valid",false);
    $("#gateway_amount_invalid").text("invalid format ( should be 0.00 )");
    return;
  }

  if( LIMIT*FAIR_FREEVISION_PRICE < obj.value ){
    $(obj).toggleClass("is-invalid",true);
    $(obj).toggleClass("is-valid",false);
    $("#gateway_amount_invalid").text("limit exceed");
    return;
  }

  $(obj).toggleClass("is-valid", true);
  $(obj).toggleClass("is-invalid",false);
  $(".line3").toggleClass("d-none",false);

}

function check_reference(obj){
  if( obj.value.toString().length > 3 && $("#gateway_amount").val() > 0 ){
    $("#gateway_calculate").toggleClass("d-none",false);
    //$("#gateway_amount").prop("readonly",true);
    $("#gateway_concept").toggleClass("is-valid",true);
    $("#gateway_concept").toggleClass("is-invalid",false);
  } else {
    $("#gateway_calculate").toggleClass("d-none",true);
    //$("#gateway_amount").prop("readonly",false);
    $("#gateway_concept").toggleClass("is-valid",false);
    $("#gateway_concept").toggleClass("is-invalid",true);
  }
}


var token = function() {
    return Math.random().toString(36).substr(2); // remove `0.`
};

function gateway_calc(){

  $("#gateway_amount").prop("readonly",true);
  $("#gateway_concept").prop("readonly",true);

  $("#gateway_amount").val( parseFloat($("#gateway_amount").val()).toFixed(3) );

  console.log( new Date(), 'calculate', parseFloat( $("#gateway_amount").val() ).toFixed(3) );

  $.post("functions/create_order.php",
    {
        action: "calculate",
        iban: $("#gateway_id").val().replace(/ /g,"").toUpperCase(),
        eur_amount: parseFloat( $("#gateway_amount").val() ).toFixed(2),
        concept: $("#gateway_concept").val().trim()
    },
    function(data, status){
      if( status != "success") alert("Data: " + data + "\nStatus: " + status);
      console.log( new Date(), data );
      var A=JSON.parse( data );

      if( parseFloat(A.limitation).toFixed(2) != LIMIT.toFixed(2) ){
        alert(parseFloat(A.limitation) + "|" + parseFloat( $("#payment_limit").text())+ " Limit client side doesn't match with server side! Please contact Admin");
        return;
      }
      if( parseFloat( A.exchange_price ) != parseFloat( $("#exchange_price").text()) ){
        alert( parseFloat( A.exchange_price ) + "|" + parseFloat( $("#exchange_price").text()) + " Exchange price client side doesn't match with server side! Please contact Admin");
        return;
      }

      $("#fair_amount").val( parseFloat(A.fair_amount).toFixed(8));
      $(".faircoin_uri").toggleClass("d-none",true);
      $("#tokeninfo").toggleClass("d-none",true);
      $(".line4").toggleClass("d-none",false);
      $("#gateway_calculate").toggleClass("d-none",true);
      $("#gateway_open").toggleClass("d-none",false);

   });
}

var opencnt=0;  // prevent double click after calculate

function gateway_start(){

  if( $("#gateway_open").hasClass("btn-success") == true ){
    alert("Stargate is already open! Now you need to pay FAIR if not yet done!");
    return;
  }

  opencnt++;

  if( opencnt < 2 || opencnt > 2 ) return;

  console.log( new Date(), 'gateway_start()', 'open' );

  $.post("functions/create_order.php",
    {
        action: "open",
        iban: $("#gateway_id").val().replace(/ /g,"").toUpperCase(),
        eur_amount: parseFloat( $("#gateway_amount").val() ).toFixed(3),
        concept: $("#gateway_concept").val().trim()
    },
    function(data, status){
      if( status != "success") alert("Data: " + data + "\nStatus: " + status);
      console.log( new Date(), 'gateway_start()', data );
      if( data == 'false' ){
         alert('Order creation not allowed! Maybe daily limit reached! Please wait one or two days and try it again!');
      } else {
        var A=JSON.parse( data );
      }


      if( parseFloat(A.limitation).toFixed(2) != LIMIT.toFixed(2) ){
        alert(parseFloat(A.limitation) + "|" + parseFloat( $("#payment_limit").text()) + " Limit client side doesn't match with server side! Please contact Admin");
        return;
      }
      if( parseFloat( A.exchange_price ) != parseFloat( $("#exchange_price").text()) ){
        alert( parseFloat( A.exchange_price ) + "|" + parseFloat( $("#exchange_price").text()) + " Exchange price client side doesn't match with server side! Please contact Admin");
        return;
      }

      $("#fair_amount").val( parseFloat(A.fair_amount).toFixed(8));
      $("#fair_address").val(A.fair_address);
      $(".faircoin_uri").toggleClass("d-none",false);


      $("#gateway_open").toggleClass("btn-primary",false);
      $("#gateway_open").toggleClass("btn-success",true);
      $("#gateway_open").text("Stargate is now open");
      $(".line4").toggleClass("d-none",false);
      $(".line5").toggleClass("d-none",false);

      $(".line1").toggleClass("goBackground",true);
      $(".line2").toggleClass("goBackground",true);
      $(".line3").toggleClass("goBackground",true);
      makeCode("faircoin://" + A.fair_address + "?amount=" + parseFloat(A.fair_amount).toFixed(8));

      $("#faircoin_uri").prop("href","faircoin://" + A.fair_address + "?amount=" + parseFloat(A.fair_amount).toFixed(8));
      $("#order_id").text( A.order_id );
      $("#tokeninfo").toggleClass("d-none",false);

      ChangeUrl("FairCoin Stargate", STARGATE_URL+"index.php?order_id=" + A.order_id);

   });

   setTimeout( function(){ checkPayment(); }, 10000 );
   console.log( new Date(), 'gateway_start()', 'CheckPayment() will executed with delay of 60 seconds.' );
}

function checkPayment(){

  console.log( new Date(), 'checkPayment()', 'executing ...');
  $.get("functions/notification.php",
    {
        order_id: $("#order_id").text()
    },
    function(data, status){
      if( status != "success") alert("Data: " + data + "\nStatus: " + status);
      console.log( new Date(), 'checkPayment()', 'notification.php', data);
      var row=JSON.parse(data);

      var w='0%';
      var v=0;
      var l='unkown data - '+row.status;
      switch( row.status ){
        case '00':
          w='20%';
          v=1;
          if( (row.botc_pay_in_timestamp+row.botc_pay_in_timeout) < new Date().getTime()/1000 ){
            // timeout
            $('#status').toggleClass('bg-success',false).toggleClass('bg-danger',true);
            $('.line4').toggleClass('d-none',true);
            l='YOUR FairCoin payment TIMED-OUT!';
          } else {
            l='Waiting for YOUR FairCoin payment!';
          }
        break;
        case '01':
          w='40%';
          v=2;
          l='Waiting for FairCoin blockchain confirmation!';
        break;
        case 'E02':
          w='60%';
          v=3;
          l='Error: SEPA transfer failed! Please contact admin t.me@TonyFord';
          $('#status').toggleClass('bg-success',false).toggleClass('bg-danger',true);
        break;
        case '03':
          w='80%';
          v=4;
          l='SEPA sent, Waiting for confirmation!<br>( In principle your order is successfully processed but notice your order_id and check it again tomorrow )';
        break;
        case '04':
          w='100%';
          v=5;
          l='SEPA transaction confirmed successfully!';
        break;
      }

      $('#status').css('width',w).attr('aria-valuenow',v).html(l);
      if( row.status != '03' && row.status != '04' ){
        setTimeout( function(){ checkPayment(); }, 60000 );
        console.log( new Date(), 'checkPayment()', 'checkPayment() will executed again delay of 60 seconds' );
      } else {
        console.log( new Date(), 'checkPayment()', 'finished status = '+row.status );
      }
   });

}

function ChangeUrl(page, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = { Page: page, Url: url };
        history.pushState(obj, obj.Page, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}

function makeCode (elText) {

	if (!elText) {
		alert("Input a text");
		elText.focus();
		return;
	}

	qrcode.makeCode(elText);
}



function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       if( variable == "coin" ){ return "btc"; } else { return(false); }

}


function json_load( url, type ){
  var json = null;
  $.ajax({
      'type':"GET",
      'async': false,
      'global': false,
      'cache': false,
      'url': url,
      'dataType': type,
      'success': function (data) {
          json = data;
      }
  });
  return json;
}
