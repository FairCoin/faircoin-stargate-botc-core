<?php

include_once('connect.php');

if( empty($_GET['order_id']) || empty($_GET['trigger']) ){
  add_log(
    Array(
      'logfile' => 'error',
      'source' => [__FILE__,__METHOD__,__LINE__],
      'info' => 'EMPTY params',
      'object' => [ 'order_id' => $_GET['order_id'], 'trigger' => $_GET['trigger'] ]
    )
  );
  exit;
}

$order_id = $_GET['order_id'];
$trigger = $_GET['trigger'];

if( $trigger != 'in' && $trigger != 'out' ){
  add_log(
    Array(
      'logfile' => 'error',
      'source' => [__FILE__,__METHOD__,__LINE__],
      'info' => 'BOTC bad trigger',
      'object' => [ 'order_id' => $_GET['order_id'], 'trigger' => $_GET['trigger'] ]
    )
  );
  exit;
}

$ignore=botc_ignore_notification_reset($order_id);

add_log(
  Array(
    'logfile' => 'tx',
    'source' => [__FILE__,__METHOD__,__LINE__],
    'info' => 'BOTC triggered',
    'object' => [ 'order_id' => $_GET['order_id'], 'trigger' => $_GET['trigger'], 'ignored' => $ignore ]
  )
);

// check if botc notification wants to be ignored
if( $ignore ) exit;

$timeout = ($trigger == 'in') ? TIMEOUT_PAY_IN : TIMEOUT_PAY_OUT;

update_trigger_by_order_id($order_id,
  Array(
    'botc_pay_trigger' => $trigger,
    'botc_pay_'.$trigger.'_timeout' => $timeout
  )
);

?>
