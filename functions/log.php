<?php

function get_path_from_file($file){
  $P=preg_split('/\//', $file);
  $flag=False; $d='';
  foreach( $P as $p ){
    if($p == 'functions' ){
      $flag=True;
    }
    if($flag){
      $d.='/'.$p;
    }
  }
  return $d;
}

function add_log( $LOG ){

  $f='log/'.date('Y-m-d',time()).'-'.$LOG['logfile'].'.log';

  if(!file_exists($f)){
    $fp=fopen( $f,'w+');
    fwrite($fp,$LOG['logfile'].PHP_EOL.'------------------------------'.PHP_EOL.PHP_EOL);
    fclose($fp);
  }

  $source_path=get_path_from_file($LOG['source'][0]);
  if($source_path == '') $source_path=$LOG['source'][0];

  $method=$LOG['source'][1];
  $line=$LOG['source'][2];

  $timestamp=time();
  $datetime=date('Y-m-d H:i:s.', $timestamp );
  $datetime.=substr( explode(' ', microtime())[0], 2,4);

  $new_log=$datetime.PHP_EOL.PHP_EOL;
  $new_log.='FILE: '.$source_path.PHP_EOL;
  $new_log.='METHOD: '.$method.PHP_EOL;
  $new_log.='LINE: '.$line.PHP_EOL.PHP_EOL;
  $new_log.='INFO: '.$LOG['info'].PHP_EOL.PHP_EOL;
  $new_log.='OBJECT: '.json_encode($LOG['object']).PHP_EOL.PHP_EOL;
  $new_log.='------------------------------'.PHP_EOL;

  $fp=fopen( $f,'a+');
  fwrite($fp,$new_log);
  fclose($fp);

}

?>
