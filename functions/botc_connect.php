<?php

  define('APICLIENT', Array(
      'grant_type' => 'password',
      'username'  => '<myusername>',
      'password'  => '<myaccountpassword>',
      'client_id' => '<clientID>',
      'client_secret' => '<clientSecret>'
    )
  );

  define('BOTC_API','https://api.bankofthecommons.coop');

  define('TX_EXPIRED',900);
  define('TX_CONFIRMATIONS',1);

  define('TX_URL_NOTIFICATION','https://'.$_SERVER['HTTP_HOST'].'/stargate/functions/botc_notification.php');
  define('PAYMENT_REQUEST_PARAMS','?trigger=in&order_id=');
  define('SENDSEPA_PARAMS','?trigger=out&order_id=');

  define('TIMEOUT_PAY_IN',900);
  define('TIMEOUT_PAY_IN_RETRY',1);

  define('TIMEOUT_PAY_OUT',43200);
  define('TIMEOUT_PAY_OUT_RETRY',6);

?>
