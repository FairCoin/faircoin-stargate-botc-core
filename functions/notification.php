<?php

include_once('connect.php');

if( ! empty($_GET['order_id']) ){

  // trigger check
  $order_id=$_GET['order_id'];

  add_log(
    Array(
      'logfile' => 'tx',
      'source' => [__FILE__,__METHOD__,__LINE__],
      'info' => 'CHECK by order_id',
      'object' => ['order_id' => $order_id ]
    )
  );

  echo check_tx($order_id);

} else {

  // timeout check

  // transaction ( in/out ) successfull, set timeout to 0
  update_botc_success('in');
  update_botc_success('out');

  // set transaction ( in/out ) timed out
  update_botc_timed_out('in');
  update_botc_timed_out('out');

  // check timed out transaction, get status by botc API
  $ROWS=get_all_entries();
  foreach($ROWS as $row){
    if( ( $row['botc_pay_trigger'] == 'in' && $row['botc_pay_in_timeout'] == 1 ) || ( $row['botc_pay_trigger'] == 'out' && $row['botc_pay_out_timeout'] == 1 ) ){
      add_log(
        Array(
          'logfile' => 'tx',
          'source' => [__FILE__,__METHOD__,__LINE__],
          'info' => 'CHECK ALL (timed-out)',
          'object' => [
            'order_id' => $row['order_id'],
            'check-in' => ( $row['botc_pay_trigger'] == 'in' && $row['botc_pay_in_timeout'] == 1 ),
            'check-out' => ( $row['botc_pay_trigger'] == 'out' && $row['botc_pay_out_timeout'] == 1 )
          ]
        )
      );
      check_tx( $row['order_id'] );
    }
  }
}


function check_tx( $order_id ){

  $row=get_by_order_id($order_id);

  if ( $row ) {
    if( $row['botc_pay_trigger'] == 'in' || $row['botc_pay_trigger'] == 'out' ){
      add_log(
        Array(
          'logfile' => 'tx',
          'source' => [__FILE__,__METHOD__,__LINE__],
          'info' => 'CHECK ALL (triggered)',
          'object' => [
            'order_id' => $row['order_id'],
            'trigger' => $row['botc_pay_trigger']
          ]
        )
      );
      botc_update_tx_data( $row );
    }
  }

  return json_encode($row);

}

function botc_update_tx_data($row){

  $botc_tx_id=( $row['botc_pay_trigger'] == 'in' ) ? $row['botc_pay_in_id'] : $row['botc_pay_out_id'];

  $botc = new BotC;
  $TX=$botc->getTx($botc_tx_id,$row['botc_pay_trigger'].'/'.(( $row['botc_pay_trigger'] == 'in' ) ? 'fac' : 'sepa' ) );

  $status='??'; $B=Array();

  add_log(
    Array(
      'logfile' => 'tx',
      'source' => [__FILE__,__METHOD__,__LINE__],
      'info' => 'UPDATE',
      'object' => [ 'row' => $row, 'botcTX' => $TX ]
    )
  );

  if( $row['botc_pay_trigger'] == 'in' ){
    // trigger set by botc notification -> get current order status

    if( $TX['status'] == 'created' ){
      $status='00';
    }

    if( $TX['status'] == 'received' ){
      $status='01';
    }

    if( $TX['status'] == 'success' ){
      $status='02';
    }

    if( $row['status'] == $status ){
      // if status has not changed then retry it later
      $result=increase_retry($row);
    } else {

      // status changed --> update status and start next actions

      if( $status == '02' ){

        // faircoin payment in is done -> create sepa pay out
        // ignore first botc notification
        botc_ignore_notification($row['order_id']);

        $SendSEPA=new SendSEPA;
        $SendSEPA->create($row['eur_amount'],$row['beneficiary'],$row['iban'], $row['bic'],$row['concept'],$row['order_id']);
        $PAY_OUT=$botc->send_SEPA( $SendSEPA );

        add_log(
          Array(
            'logfile' => 'tx',
            'source' => [__FILE__,__METHOD__,__LINE__],
            'info' => 'UPDATE',
            'object' => [ 'status' => $status, 'row' => $row, 'SendSEPA' => $SendSEPA, 'botcPAY_OUT' => $PAY_OUT ]
          )
        );


        if( $PAY_OUT['status'] == 'sending' ){
          $status='03';
        } else {
          $status='E02';
          add_log(
            Array(
              'logfile' => 'error',
              'source' => [__FILE__,__METHOD__,__LINE__],
              'info' => 'UPDATE',
              'object' => [ 'status' => $status, 'row' => $row, 'SendSEPA' => $SendSEPA, 'botcPAY_OUT' => $PAY_OUT ]
            )
          );
        }

        // reset botc notification trigger and set timestamp and timeout
        $B['botc_pay_trigger']='';
        $B['botc_pay_out_timestamp']=time();
        $B['botc_pay_out_timeout']=TIMEOUT_PAY_OUT;
        $B['botc_pay_out_status']=$PAY_OUT['status'];
        $B['botc_pay_out_id']=$PAY_OUT['id'];
      }

      // update order status in database
      $B['status']=$status;
      $result=update_trigger_by_order_id($row['order_id'],$B);

    }

  } else if( $row['botc_pay_trigger'] == 'out' ){
    // trigger botc notification is set -> get current order status

    if( $TX['status'] == 'sending' ){
      $status='03';
    }

    if( $TX['status'] == 'success' ){
      $status='04';
    }

    if( $row['status'] == $status ){
      // if status has not changed then retry it later
      $result=increase_retry($row);
    } else {
      // update order status in database
      $B['status']=$status;
      $result=update_trigger_by_order_id($row['order_id'],$B);
    }

  }

  if ( $result ) {
      // success
  } else {
      // error return false
      add_log(
        Array(
          'logfile' => 'error',
          'source' => [__FILE__,__METHOD__,__LINE__],
          'info' => 'UPDATE',
          'object' => [ 'row' => $row, 'status' => $status ]
        )
      );
  }

}

function increase_retry($row){

  $timeout = ($row['btc_pay_trigger'] == 'in') ? TIMEOUT_PAY_IN : TIMEOUT_PAY_OUT;
  $retry   = ($row['btc_pay_trigger'] == 'in') ? TIMEOUT_PAY_IN_RETRY : TIMEOUT_PAY_OUT_RETRY;

  if( $row['botc_pay_retry'] >= $retry ){
    // retry count reached -> set status error
    add_log(
      Array(
        'logfile' => 'error',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'RETRY LIMIT',
        'object' => [ 'row' => $row, 'retry' => $retry ]
      )
    );
    $result=update_status_by_order_id( $row['order_id'], $row['status'], 'E'.$row['status'] );
  } else {
    // increase retry count and set timeout again
    $B=Array(
      'botc_pay_'.$row['botc_pay_trigger'].'_timeout' => time()+$timeout,
      'botc_pay_retry' => $row['botc_pay_retry']+1
    );

    add_log(
      Array(
        'logfile' => 'tx',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'RETRY',
        'object' => [ 'row' => $row, 'B' => $B, 'retry' => $retry ]
      )
    );

    $result=update_trigger_by_order_id( $row['order_id'], $B );
  }

  if (!$result) {
    add_log(
      Array(
        'logfile' => 'error',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'UPDATE',
        'object' => [ 'row' => $row ]
      )
    );
  }

  return $result;

}




?>
