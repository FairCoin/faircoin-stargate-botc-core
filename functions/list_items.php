<?php

include_once(($path='./').'connect.php');

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>FairCoin Stargate
    </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">
    <link href="../assets/css/parallax.css" rel="stylesheet">
    <link href="../assets/css/bootswatch_solar.css" rel="stylesheet">
    <link href="../assets/css/demo.css" rel="stylesheet">
    <link href="../assets/css/component.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Raleway:200,400,800' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../assets/css/bootswatch_solar.css" rel="stylesheet">
    <style>
    .list_items_headline {
      background:rgba(0,0,0,0.5);
    }
    .list_items_table {
      border-bottom:1px solid rgba(0,0,0,0.5);
    }

    button {
      margin-bottom:0.3em;
    }
    </style>
  </head>
  <body>
  <script src="../assets/js/jquery.min.js"></script>
  <script src="../assets/js/bootstrap.min.js"></script>
  <script src="../assets/js/bootstrap.bundle.min.js"></script>

  <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
  <div id='stars'></div>
  <div id='stars2'></div>
  <div id='stars3'></div>

  <div class="container-fluid">
    <div class="demo-1">
      <div class="content">
        <div id="large-header" class="large-header" style="height: 100%;">
          <canvas id="demo-canvas" width="1323" height="402"></canvas>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <input id="authcode" type="text" class="form-control">
      </div>
    </div>
    <div class="row">
      <div id="items" class="col">

      </div>
    </div>
  </div>

  <script>



  var J={};
  var JJ={};

  var STARGATE_URL='<?= STARGATE_URL ?>';

  $( document ).ready(function() {
    $('#authcode').keyup(
      function(e){
        if(e.keyCode == 13){

          $.post('get_orders_json.php',
            {
                code: $(this).val(),
            },
            function(data, status){
              if( status != 'success') alert('Data: ' + data + '\nStatus: ' + status);
              JJ=JSON.parse(data);
              console.log(JJ);
              J=json_load( STARGATE_URL + '/data/stargate_entries.json','json' );
              list_items();
           });
        }
      }
    );

  });


  </script>

<script src="../assets/js/TweenLite.min.js"></script>
<script src="../assets/js/EasePack.min.js"></script>
<script src="../assets/js/rAF.js"></script>
<script src="../assets/js/demo-1.js"></script>
<script src="../assets/js/list_items.js"></script>
</body>
</html>
