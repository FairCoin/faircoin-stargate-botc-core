<?php

include('token.php');
include('connect.php');

if( empty($_POST['iban']) || empty($_POST['eur_amount']) || empty($_POST['concept']) || empty($_POST['action']) ){

  add_log(
    Array(
      'logfile' => 'error',
      'source' => [__FILE__,__METHOD__,__LINE__],
      'info' => 'not all params posted',
      'object' => ['iban' => $_POST['iban'], 'eur_amount' => $_POST['eur_amount'], 'concept' => $_POST['concept'],'action' => $_POST['action'] ]
    )
  );
  exit;

}

$action   = $_POST['action'];
$iban     = $_POST['iban'];
$eur_amount = $_POST['eur_amount'];
$concept  = $_POST['concept'];


$A=array();


if( $action == 'calculate' ){

  $row=get_by_iban($iban);

  if( $row ){
    $A['limitation'] = LIMIT * (( LIMIT == DEFAULT_LIMIT ) ? $row['limit_multiplier'] : 1 );
    $A['exchange_price'] = FAIR_FREEVISION_PRICE;
    $A['fair_amount'] = number_format( $eur_amount / FAIR_FREEVISION_PRICE,8,'.','' );
    echo json_encode($A);
  } else {
    echo 'false';
  }

} else if( $action == 'open' ){

  // check request limitation by ip address
  $usersIPlog='log/ips/'.$_SERVER['REMOTE_ADDR'].'.log';
  if( file_exists($usersIPlog) ){
    $fp=fopen($usersIPlog,'r');
    $crCnt=fread($fp,filesize($usersIPlog));
    fclose($fp);
    if( $crCnt >= USER_ORDER_CREATION_LIMIT_DAILY ){
      echo 'false';
      exit;
    } else {
      $crCnt++;
    }
  } else {
    $crCnt=1;
  }

  $row=get_by_iban($iban);

  if( $row ) {
    // output data of each row
    $A['limitation'] = LIMIT * (( LIMIT == DEFAULT_LIMIT ) ? $row['limit_multiplier'] : 1 );
    $A['exchange_price'] = FAIR_FREEVISION_PRICE;
    $A['fair_amount'] = number_format( $eur_amount / $A['exchange_price'],8,'.','' );


    // create tx
    $order_id=getToken(6);
    $A['order_id']=$order_id;

    // create order initially ( prevent botc_notification.php cant find order instantly after receive_FAIR payment request )
    botc_ignore_notification($order_id);

    $botc=new BotC;

    $PaymentRequest=new PaymentRequest;
    $PaymentRequest->create( $A['fair_amount'], $order_id , $order_id);
    $PAY_IN=$botc->receive_FAIR( $PaymentRequest );

    // set limitation of order creation by ip address
    $fp=fopen($usersIPlog,'w+');
    fwrite($fp,(string)$crCnt);
    fclose($fp);

    $botc_pay_in_id = $PAY_IN['id'];
    $botc_pay_in_status = $PAY_IN['status'];
    $A['fair_address'] = $PAY_IN['pay_in_info']['address'];

    $AA=Array(
      'order_id' => $order_id,
      'beneficiary' => $row['beneficiary'],
      'iban' => $row['iban'],
      'bic' => $row['bic'],
      'eur_amount' => $eur_amount,
      'concept' => $concept,
      'fair_address' => $A['fair_address'],
      'fair_amount' => $A['fair_amount'],
      'exchange_price' => $A['exchange_price'],
      'status' => '00',
      'botc_pay_in_id' => $botc_pay_in_id,
      'botc_pay_in_status' => $botc_pay_in_status,
      'botc_pay_in_timestamp' => time(),
      'botc_pay_in_timeout' => time()+TIMEOUT_PAY_IN
    );

    add_log(
      Array(
        'logfile' => 'tx',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'CREATE',
        'object' => ['AA' => $AA, 'botcPAY_IN' => $PAY_IN ]
      )
    );

    $result=add_order( $AA );

    include('reset_available_balance.php');

    if($result){ echo json_encode($A); } else { echo 'false'; }

  } else {
    echo 'false';
  }

}

?>
