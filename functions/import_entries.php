<?php

include_once('connect.php');

function implode_categories($A){
  $cats='';
  foreach($A['categories'] as $cat=>$active){
    if($active == 1){
      $cats.='#'.$cat.' ';
    }
  }
  $A['categories']=$cats;
  return $A;
}

$F=glob('../data/stargate_iban_updates/*.json');

foreach($F as $f){

  $fp=fopen($f,'r');
  $json=fread($fp,filesize($f));
  fclose($fp);

  $JS=json_decode($json,true );

  if( empty($JS['action']) || empty($JS['name']) || empty($JS['categories']) || empty($JS['url']) || empty($JS['iban']) || empty($JS['bic']) || empty($JS['latitude']) || empty($JS['longitude']) ){
  } else {
    if($JS['action'] == 'update'){
      unset($JS['action']);
      $JS['beneficiary']=preg_replace( '/[^A-Za-z0-9]+/',' ', $JS['beneficiary'] );
      $JS['iban']=preg_replace('/ /','',$JS['iban']);
      $JS['bic']=preg_replace('/ /','',$JS['bic']);
      $result=add_entry(implode_categories($JS));
      if( $result ) {
        echo $JS['name'].' updated<br>';
        unlink($f);
      } else {
        echo $JS['name'].' failed<br>';
      }
    }
  }
}

include('update_umap_json.php');

?>
