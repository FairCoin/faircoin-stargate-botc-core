<?php
include_once('botc_connect.php');
include_once('botc_api.php');

include_once('config.php');
include_once('log.php');
include('reset_ips.php');

//#### config.php must included before this part getting constant DATASTORAGE
include_once(DATASTORAGE.'/add_entry.php');
include_once(DATASTORAGE.'/add_order.php');
include_once(DATASTORAGE.'/botc_ignore_notification.php');
include_once(DATASTORAGE.'/botc_ignore_notification_reset.php');
include_once(DATASTORAGE.'/db_connect.php');
include_once(DATASTORAGE.'/get_all_entries.php');
include_once(DATASTORAGE.'/get_all_orders.php');
include_once(DATASTORAGE.'/get_by_iban.php');
include_once(DATASTORAGE.'/get_by_order_id.php');
include_once(DATASTORAGE.'/get_reserved_balance.php');
include_once(DATASTORAGE.'/update_botc_success.php');
include_once(DATASTORAGE.'/update_botc_timed_out.php');
include_once(DATASTORAGE.'/update_status_by_order_id.php');
include_once(DATASTORAGE.'/update_trigger_by_order_id.php');

ob_start();
include_once('get_available_balance.php');
$av_balance = ob_get_clean();

define('LIMIT', (($av_balance < (DEFAULT_LIMIT*FAIR_FREEVISION_PRICE) ) ? (intVal($av_balance)/FAIR_FREEVISION_PRICE) : DEFAULT_LIMIT ));

?>
