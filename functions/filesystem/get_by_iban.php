<?php

function get_by_iban($iban){

  $iban=preg_replace('/ /','',$iban);
  $f='../data/stargate_iban_entries/'.base58::StringEncode($iban).'.encoded';
  if(!file_exists($f)){
    $f='../faircoin-stargate-botc-data/stargate_iban_entries/'.base58::StringEncode($iban).'.encoded';
  }

  if(file_exists($f)){
    $fp=fopen($f,'r');
    $encoded=fread($fp,filesize($f));
    fclose($fp);
    $result=base58::StringDecode($encoded);
    $result=json_decode($result,true);
  } else {
    $result=false;
  }

  return $result;

}

?>
