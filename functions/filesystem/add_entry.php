<?php

function add_entry($A){

  $A['iban']=preg_replace('/ /','',$A['iban']);
  $A['bic']=preg_replace('/ /','',$A['bic']);

  $f='../data/stargate_iban_entries/'.base58::StringEncode($A['iban']).'.encoded';

  $encoded=base58::StringEncode(json_encode($A));

  $fp=fopen($f,'w+');
  fwrite($fp,$encoded);
  fclose($fp);

  add_log(
    Array(
      'logfile' => 'entry',
      'source' => [__FILE__,__METHOD__,__LINE__],
      'info' => 'UPDATE',
      'object' => ['A' => $A, 'entry_id' => base58::StringEncode($A['iban']) ]
    )
  );

  return true;

}

?>
