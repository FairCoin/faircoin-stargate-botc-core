<?php

// no database
define('FPASSW','<mypasswordfordataencryption>');

class base58
{
	static public $alphabet = "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";

	public static function encode($int) {
		$base58_string = "";
		$base = strlen(self::$alphabet);
		while($int >= $base) {
			$div = floor($int / $base);
			$mod = ($int - ($base * $div)); // php's % is broke with >32bit int on 32bit proc
			$base58_string = self::$alphabet{$mod} . $base58_string;
			$int = $div;
		}
		if($int) $base58_string = self::$alphabet{$int} . $base58_string;
		return $base58_string;
	}

	public static function decode($base58) {
		$int_val = 0;
		for($i=strlen($base58)-1,$j=1,$base=strlen(self::$alphabet);$i>=0;$i--,$j*=$base) {
			$int_val += $j * strpos(self::$alphabet, $base58{$i});
		}
		return $int_val;
	}

  public static function StringEncode($string) {
    $A=str_split($string);
    $HEX=[];
    $base58='';
    while(count($A)>0){
      $a=array_shift($A);
      $HEX[]=dechex(ord($a));
      if(count($HEX)>=6){
        $base58.=self::encode(hexdec(join($HEX))).( (count($A)>0) ? '.' : '');
        $HEX=[];
      }
    }
    $base58.=self::encode(hexdec(join($HEX)));
    return $base58;
  }

  public static function StringDecode($base58) {

    $B=preg_split('/\./',$base58);
    $string='';
    while(count($B)>0){

      $b=array_shift($B);
      $dec=self::decode($b);
      $hex=dechex($dec);
      $HEX=str_split($hex);

      while(count($HEX)>0){
        $string.=chr(hexdec(join(array_splice($HEX,0,2))));
      }

    }

    return $string;
  }

}

function encrypt( $plaintext, $password=FPASSW ) {
    $method = "AES-256-CBC";
    $key = hash('sha256', $password, true);
    $iv = openssl_random_pseudo_bytes(16);

    $ciphertext = openssl_encrypt($plaintext, $method, $key, OPENSSL_RAW_DATA, $iv);
    $hash = hash_hmac('sha256', $ciphertext, $key, true);

    return $iv . $hash . $ciphertext;
}

function decrypt( $ivHashCiphertext, $password=FPASSW ) {
    if( empty($password) ) $password=FPASSW;
    $method = "AES-256-CBC";
    $iv = substr($ivHashCiphertext, 0, 16);
    $hash = substr($ivHashCiphertext, 16, 32);
    $ciphertext = substr($ivHashCiphertext, 48);
    $key = hash('sha256', $password, true);

    if (hash_hmac('sha256', $ciphertext, $key, true) !== $hash) return null;

    return openssl_decrypt($ciphertext, $method, $key, OPENSSL_RAW_DATA, $iv);
}


?>
