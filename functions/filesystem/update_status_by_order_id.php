<?php

function update_status_by_order_id($order_id,$status,$new_status){

  $A=get_by_order_id($order_id);

  if( $A ){
    if( $A['status'] == $status ){
      $A['status']=$new_status;
      $result=add_order($A);
      add_log(
        Array(
          'logfile' => 'tx',
          'source' => [__FILE__,__METHOD__,__LINE__],
          'info' => 'UPDATE',
          'object' => [ $A, 'old_status' => $status, 'new_status' => $new_status ]
        )
      );
      $result=true;
    } else {
      add_log(
        Array(
          'logfile' => 'error',
          'source' => [__FILE__,__METHOD__,__LINE__],
          'info' => 'UPDATE - bad status',
          'object' => [ $A, 'old_status' => $status, 'new_status' => $new_status ]
        )
      );
      $result=false;
    }
  } else {
    add_log(
      Array(
        'logfile' => 'error',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'UPDATE - order not exists',
        'object' => [ $order_id ]
      )
    );
    $result=false;
  }

  return $result;

}

?>
