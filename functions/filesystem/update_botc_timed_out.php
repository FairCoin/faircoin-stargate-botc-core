<?php

function update_botc_timed_out($direction){

  // set transaction ( in/out ) timed out
  $ROWS=get_all_orders();

  foreach($ROWS as $row){
    if( $row['botc_pay_'.$direction.'_status'] != 'success' && $row['botc_pay_'.$direction.'_timeout'] < time() && $row['botc_pay_'.$direction.'_timeout'] > 1 ){
      $row['botc_pay_trigger']=$direction;
      $row['botc_pay_'.$direction.'_timeout'] = 1;
      $result=add_order($row);
      if(!$result){
        add_log(
          Array(
            'logfile' => 'error',
            'source' => [__FILE__,__METHOD__,__LINE__],
            'info' => 'UPDATE',
            'object' => $row
          )
        );
      }
    }
  }

}

?>
