<?php

function update_botc_success($direction){

  // transaction ( in ) successfull, set timeout to 0
  $ROWS=get_all_orders();

  foreach($ROWS as $row){
    if( $row['botc_pay_'.$direction.'_status'] == 'success' && $row['botc_pay_'.$direction.'_timeout'] > 0 ){
      $row['botc_pay_'.$direction.'_timeout'] = 0;
      $result=add_order($row);
      if(!$result){
        add_log(
          Array(
            'logfile' => 'error',
            'source' => [__FILE__,__METHOD__,__LINE__],
            'info' => 'UPDATE',
            'object' => $row
          )
        );
      }
    }
  }

}

?>
