<?php

function get_by_order_id($order_id){

  $f='../data/stargate_iban_orders/'.base58::StringEncode($order_id).'.encoded';

  add_log(
    Array(
      'logfile' => 'getbyid',
      'source' => [__FILE__,__METHOD__,__LINE__],
      'info' => 'GET',
      'object' => ['order_id' => $order_id, 'sleep' => $sleep, 'file' => $f ]
    )
  );

  if(file_exists($f)){
    $fp=fopen($f,'r');
    $encoded=fread($fp,filesize($f));
    fclose($fp);
    $decoded=base64_decode($encoded);
    $decoded=decrypt($decoded,FPASSW);
    $result=json_decode($decoded,true);
  } else {
    $result=false;
  }

  return $result;

}

?>
