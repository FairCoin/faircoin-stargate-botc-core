<?php

function update_trigger_by_order_id($order_id,$B){

  $A=get_by_order_id($order_id);

  add_log(
    Array(
      'logfile' => 'tx',
      'source' => [__FILE__,__METHOD__,__LINE__],
      'info' => 'UPDATE',
      'object' => [ 'order_id' => $order_id, 'A' => $A, 'B' => $B ]
    )
  );

  if( $A ){
    $A=array_merge($A,$B);
    $result=add_order($A);
    $result=true;
  } else {
    add_log(
      Array(
        'logfile' => 'error',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'UPDATE',
        'object' => ['A' => $A, 'B' => $B ]
      )
    );
    $result=false;
  }
  return $result;
}

?>
