<?php

function botc_ignore_notification_reset($order_id){

  $f='../data/botc_ignore_notification/'.base58::StringEncode($order_id).'.ignore';

  add_log(
    Array(
      'logfile' => 'botcnotification',
      'source' => [__FILE__,__METHOD__,__LINE__],
      'info' => 'RESET',
      'object' => [ 'order_id' => $order_id, 'file' => $f ]
    )
  );

  if(file_exists($f)){
    unlink($f);
    return true;
  } else {
    return false;
  }

}

?>
