<?php

function botc_ignore_notification($order_id){

  $f='../data/botc_ignore_notification/'.base58::StringEncode($order_id).'.ignore';

  add_log(
    Array(
      'logfile' => 'botcnotification',
      'source' => [__FILE__,__METHOD__,__LINE__],
      'info' => 'IGNORE',
      'object' => [ 'order_id' => $order_id, 'file' => $f ]
    )
  );

  $fp=fopen($f,'w+');
  fwrite($fp,'');
  fclose($fp);

  return true;

}

?>
