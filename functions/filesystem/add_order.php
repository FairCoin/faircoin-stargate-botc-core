<?php

function add_order($A){

  $f='../data/stargate_iban_orders/'.base58::StringEncode($A['order_id']).'.encoded';

  $encoded=json_encode($A);
  $encoded=encrypt($encoded);
  $encoded=base64_encode($encoded);

  $fp=fopen($f,'w+');
  fwrite($fp,$encoded);
  fclose($fp);

  add_log(
    Array(
      'logfile' => 'tx',
      'source' => [__FILE__,__METHOD__,__LINE__],
      'info' => 'UPDATE',
      'object' => $A
    )
  );

  return true;
}

?>
