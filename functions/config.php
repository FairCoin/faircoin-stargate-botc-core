<?php

define('STARGATE_URL','<urltostargate>');
define('DATASTORAGE', 'filesystem');

define('DEFAULT_LIMIT',100000 );  // in FairCoin

define('FAIR_FREEVISION_PRICE', json_decode( file_get_contents($path.'../data/freevision_price.json'),true )['fv_bid']);

define('AUTH', Array(
    Array(
      "id" =>  "@<2FA.username>",
      "key" => "<2FA.key>"
    )
  )
);

define('AUTH_TIME_OFFSET_READ',20); // n * 30sec
define('AUTH_TIME_OFFSET_WRITE',2); // n * 30sec

?>
