<?php

include_once('connect.php');
include_once('GoogleAuthenticator/GoogleAuthenticator.php');

$order_id = empty( $_POST['order_id'] ) ? '' : $_POST['order_id'];
$code = empty( $_POST['code'] ) ? '' : $_POST['code'];

if( empty($order_id) ){
  $ga=new PHPGangsta_GoogleAuthenticator;
  foreach ( AUTH as $auth ){
     if($ga->verifyCode( $auth['key'], $code, AUTH_TIME_OFFSET_READ ) ) {
       $A=get_all_orders();
     }
  }
} else {
  $row=get_by_order_id($order_id);
  if($row){
    $A=[get_by_order_id($order_id)];
  } else {
    $A=[];
  }
}

if( count($A)>0 ){
  // convert numbers from string to float
  foreach ( $A as $i=>$v ){
    $A[$i]['amount']=$v['amount']*1.00;
    $A[$i]['fair_amount']=$v['fair_amount']*1.00000000;
    $A[$i]['beneficiary']=utf8_encode($A[$i]['beneficiary']);
    $A[$i]['concept']=utf8_encode($A[$i]['concept']);
  }
}

echo json_encode($A);

?>
