<?php

include_once($path.'connect.php');

$filename=$path.'../data/available_balance.txt';

$upd=false;


if(file_exists($filename)){

  $fp=fopen($filename,'r');
  $data = fread($fp,filesize($filename));
  fclose($fp);
  if( filemtime( $filename ) < time()-3600 ) $upd=true;

} else {
  $upd=true;
}

if( $upd ) {

  $botc=new BotC;
  $botc->getBalances();
  $balance_account=$botc->balances['EUR'];
  $botc=Null;

  $reserved_balance=get_reserved_balance();
  $data=$balance_account-$reserved_balance;

  $fp=fopen($filename,'w+');
  fwrite($fp,$data);
  fclose($fp);

}

echo $data;

?>
