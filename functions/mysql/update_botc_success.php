<?php

function update_botc_success($direction){

  $db=new db;

  // transaction ( in ) successfull, set timeout to 0
  $sql = "UPDATE stargate_iban_orders SET botc_pay_".$direction."_timeout=0 WHERE botc_pay_".$direction."_status = 'success'";
  if( $db->query($sql) === TRUE ){
    add_log(
      Array(
        'logfile' => 'tx',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'UPDATE',
        'object' => [ $sql ]
      )
    );
    $result=true;
  } else {
    add_log(
      Array(
        'logfile' => 'error',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'UPDATE',
        'object' => [ $sql ]
      )
    );
    $result=false;
  }
  unset($db);
  return $result;
}

?>
