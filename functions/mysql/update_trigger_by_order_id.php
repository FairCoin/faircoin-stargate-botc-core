<?php


function update_trigger_by_order_id($order_id,$B){

  $db=new db;

  foreach($B as $i=>$b){
    $BB[]=$i."='".$b."'";
  }

  $sql = "UPDATE stargate_iban_orders SET ".implode(', ',$BB)." WHERE order_id='".$order_id."'";

  if( $db->query($sql) === TRUE ){
    add_log(
      Array(
        'logfile' => 'tx',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'UPDATE',
        'object' => [ $sql ]
      )
    );
    $result=true;
  } else {
    add_log(
      Array(
        'logfile' => 'error',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'UPDATE',
        'object' => [ $sql ]
      )
    );
    $result=false;
  }

  unset($db);
  return $result;
}


?>
