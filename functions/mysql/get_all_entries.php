<?php

function get_all_entries(){

  $db=new db;

  $sql = 'SELECT * FROM stargate_iban_entries ORDER BY name ASC';
  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ) $ROW[]=$row;

  unset($db);
  return $ROW;

}

?>
