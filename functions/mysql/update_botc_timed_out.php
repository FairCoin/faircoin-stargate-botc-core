<?php

function update_botc_timed_out($direction){

  $db=new db;

  // set transaction ( in/out ) timed out
  $sql = "UPDATE stargate_iban_orders SET botc_pay_trigger='".$direction."', botc_pay_".$direction."_timeout=1 WHERE botc_pay_".$direction."_status != 'success' AND botc_pay_".$direction."_timeout<".time()." AND botc_pay_".$direction."_timeout>1";
  if( $db->query($sql) === TRUE ){
    add_log(
      Array(
        'logfile' => 'tx',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'UPDATE',
        'object' => [ $sql ]
      )
    );
    $result=true;
  } else {
    add_log(
      Array(
        'logfile' => 'error',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'UPDATE',
        'object' => [ $sql ]
      )
    );
    $result=false;
  }

  unset($db);
  return $result;

}

?>
