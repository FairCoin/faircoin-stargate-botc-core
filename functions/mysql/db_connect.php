<?php

class db
{
  private $servername = '<mysql.servername>';
  private $username = '<mysql.username>';
  private $password = '<mysql.password>';
  private $aes_key = '<mydataencryptionkey>';
  private $dbname = '<mysql.dbname>';
  public $conn;

  function __construct() {
    // Create connection
    $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
    // Check connection
    if ($this->conn->connect_error) {
       die('Connection failed: ' . $this->conn->connect_error);
    }
  }

  function __destruct() {
    $this->conn->close();
  }

  public function query($sql){
    $sql=preg_replace('/KEY/','"'.$this->aes_key.'"',$sql);
    return $this->conn->query($sql);
  }

  public function commit(){
    return $this->conn->commit();
  }

  public function autocommit($bool){
    return $this->conn->autocommit($bool);
  }
}

?>
