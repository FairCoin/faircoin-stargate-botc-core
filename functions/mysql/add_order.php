<?php

function add_order($A){

  $db=new db;

  $columns = implode(', ',array_keys($A));
  foreach(array_values($A) as $v){
    $escaped_values[]='"'.mysqli_real_escape_string($db->conn,$v).'"';
  }
  $values  = implode(', ', $escaped_values);

  $db->autocommit(FALSE);
  $db->query("DELETE FROM stargate_iban_orders WHERE iban='".$A['order_id']."'");
  $db->query("INSERT INTO stargate_iban_orders ($columns) VALUES ($values)");

  if ( $db->commit() === TRUE) {
      // success
      add_log(
        Array(
          'logfile' => 'tx',
          'source' => [__FILE__,__METHOD__,__LINE__],
          'info' => 'UPDATE',
          'object' => [ 'A' => $A, 'sql' => 'INSERT INTO stargate_iban_orders ($columns) VALUES ($values)' ]
        )
      );
      $result=$A;
  } else {
      // error return false
      add_log(
        Array(
          'logfile' => 'error',
          'source' => [__FILE__,__METHOD__,__LINE__],
          'info' => 'UPDATE',
          'object' => [ 'A' => $A, 'sql' => 'INSERT INTO stargate_iban_orders ($columns) VALUES ($values)' ]
        )
      );
      $result=false;
  }

  unset($db);
  return $result;

}

?>
