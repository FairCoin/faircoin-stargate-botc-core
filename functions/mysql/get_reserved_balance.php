<?php

function get_reserved_balance(){

  $db=new db;

  $sql = 'SELECT IF(SUM(eur_amount) is NULL,0,SUM(eur_amount)) AS reserved_balance FROM stargate_iban_orders WHERE status!="04"';
  $result = $db->query($sql);
  if( $result = $result->fetch_assoc() ){
    $reserved_balance=$result['reserved_balance'];
  } else {
    $reserved_balance=0;
  }

  unset($db);
  return $reserved_balance;

}

?>
