<?php

function update_status_by_order_id($order_id,$status,$new_status){

  $db=new db;
  $sql = "UPDATE stargate_iban_orders SET status='".$new_status."' WHERE order_id='".$order_id."' AND status='".$status."'";

  if( $db->query($sql) === TRUE ){
    add_log(
      Array(
        'logfile' => 'tx',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'UPDATE',
        'object' => [ $sql ]
      )
    );
    $result=true;
  } else {
    add_log(
      Array(
        'logfile' => 'error',
        'source' => [__FILE__,__METHOD__,__LINE__],
        'info' => 'UPDATE',
        'object' => [ $sql ]
      )
    );
    $result=false;
  }

  unset($db);
  return $result;
}

?>
