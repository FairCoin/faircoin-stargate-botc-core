<?php

function add_entry($A){

  $db=new db;
  $columns = implode(', ',array_keys($A));
  foreach(array_values($A) as $v){
    $escaped_values[]='"'.mysqli_real_escape_string($db->conn,$v).'"';
  }
  $values  = implode(', ', $escaped_values);

  $db->autocommit(FALSE);
  $db->query("DELETE FROM stargate_iban_entries WHERE iban='".$A['iban']."'");
  $db->query("INSERT INTO stargate_iban_entries ($columns) VALUES ($values)");
  if ( $db->commit() === TRUE) {
      // success
      add_log(
        Array(
          'logfile' => 'entry',
          'source' => [__FILE__,__METHOD__,__LINE__],
          'info' => 'UPDATE',
          'object' => [ 'A' => $A, 'sql' => 'INSERT INTO stargate_iban_entries ($columns) VALUES ($values)' ]
        )
      );
      $result=true;
  } else {
      // error return false
      add_log(
        Array(
          'logfile' => 'error',
          'source' => [__FILE__,__METHOD__,__LINE__],
          'info' => 'UPDATE',
          'object' => [ 'A' => $A, 'sql' => 'INSERT INTO stargate_iban_entries ($columns) VALUES ($values)' ]
        )
      );
      $result=false;
  }

  unset($db);
  return $result;

}

?>
