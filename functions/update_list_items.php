<?php

include_once('connect.php');

if( empty($_POST['c']) || empty($_POST['order_id']) ){
  exit;
}

$oneCode=$_POST['c'];
$order_id=$_POST['order_id'];
$status=$_POST['status'];

include_once('GoogleAuthenticator/GoogleAuthenticator.php');

$ga = new PHPGangsta_GoogleAuthenticator();

foreach ( AUTH as $auth ){
  $checkResult = $ga->verifyCode($auth['key'], $oneCode, AUTH_TIME_OFFSET_EDIT);    // 2 = 2*30sec clock tolerance
  if ($checkResult) {
    $new_status='';
    switch($status){
      case '??':
        $new_status='04';
      break;
      case 'E02':
        $new_status='04';
      break;
      case '03':
        $new_status='04';
      break;
      case 'E03':
        $new_status='04';
      break;
    }
    if( $new_status != '' ){
      if( ! update_status_by_order_id($order_id, $status,$new_status) ) echo 'FAILED_00';
    } else {
      echo 'FAILED_01';
    }

    return;
  }
}

echo 'FAILED_02';
?>
