<?php


include_once('connect.php');

$ROWS=get_all_entries();

$UMAP=Array(
  'type' => 'FeatureCollection',
  'features' => Array(Array())
);

$DATA=Array();

foreach($ROWS as $row) {
  $B=
    Array(
      'type' => 'Feature',
      'properties' =>
      Array(
        '_storage_options' =>
        Array(
          'iconUrl' => '',
          'color' => ''
        ),
        'name' => $row['name'].' ... '.$row['categories'],
        'url' => $row['url'],
        'icon' => '',
        'categories' => $row['categories'],
        'lastupdate' => substr($row['lastupdate'],0,10),
      ),
      'geometry' =>
      Array(
        'type' => 'Point',
        'coordinates' => [ floatVal($row['longitude']), floatVal( $row['latitude'] ) ]
      )
    );

    array_push($UMAP['features'][0],$B);
    array_push($DATA,$row);
}

$fp=fopen('../data/umap.geo.json','w+');
fwrite($fp,json_encode($UMAP));
fclose($fp);

$fp=fopen('../data/stargate_entries.json','w+');
fwrite($fp,json_encode($DATA));
fclose($fp);

?>
